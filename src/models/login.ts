import { Knex } from 'knex';
export class LoginService {
  checkLogin(db: Knex, username: any) {
    return db('user')
      .select('id', 'password_hash', 'roles')
      .where('username', username)
      .where('status', true)
      .first();
  }
}